exports.register = function () {
	var plugin = this;
	plugin.load_dkim_sign_ini();
	plugin.load_dkim_key();
	function load_config () {
		plugin.private_key = plugin.config.get('dkim.private.key', 'data', load_config).join('\n');
	}
};
